import pygame
import sys
import random
import time
def countdown_timer(switch_time):
    while switch_time:
        m, s = divmod(switch_time, 60)
        timer = '{:02d}:{:02d}'.format(m, s)
        print(timer, end="\r")
        time.sleep(1)
        switch_time -= 1 
    return switch_time
class FallingObject(pygame.sprite.Sprite):
    def __init__(self, screen_width, image_path, speed_range, object_type):
        super().__init__()
        self.image = pygame.transform.scale(pygame.image.load(image_path), (30, 30))
        self.rect = self.image.get_rect()
        self.rect.x = random.randint(0, screen_width - self.rect.width)
        self.rect.y = -self.rect.height
        self.speed = random.randint(*speed_range)
        self.object_type = object_type

    def update(self):
        self.rect.y += self.speed

def run_mario_jump_game():
    pygame.init()
    pygame.mixer.init()  

    # Load sound effects
    jump_sound = pygame.mixer.Sound('sounds/up.mp3')  
    catch_sound = pygame.mixer.Sound('sounds/down.mp3') 

    clock = pygame.time.Clock()
    screen_width, screen_height = 1280, 720
    screen = pygame.display.set_mode((screen_width, screen_height), pygame.RESIZABLE)
    pygame.display.set_caption("Jumping in PyGame")
    x_position, y_offset = screen_width // 2, 100  # middle of the screen
    y_position = screen_height - y_offset  # 100 pixels above the bottom
    jumping = False
    y_gravity = 1
    jump_height = 5
    y_velocity = jump_height
    move_speed = 5  # Horizontal movement speed
    standing_surface = pygame.transform.scale(pygame.image.load("assets/mario_standing.png"), (48, 64))
    jumping_surface = pygame.transform.scale(pygame.image.load("assets/mario_jumping.png"), (48, 64))
    flipped_standing_surface = pygame.transform.flip(standing_surface, True, False)
    flipped_jumping_surface = pygame.transform.flip(jumping_surface, True, False)
    background = pygame.transform.scale(pygame.image.load("assets/background.jpg"), (screen_width, screen_height))
    mario_rect = standing_surface.get_rect(center=(x_position, y_position))

    falling_objects = pygame.sprite.Group()
    #number of lives
    lives = 5
    heart_image = pygame.transform.scale(pygame.image.load("assets/heart.webp"), (30, 30))

    score = 0
    font = pygame.font.Font(None, 36)  # Font for displaying the score

    start_time = pygame.time.get_ticks()
    switch_time = 10000  # Time to switch mechanics (10 seconds)

    # Add timer variables
    timer_font = pygame.font.Font(None, 36)
    timer_color = (0, 255, 0)  # Initially set to green

    while True:
        current_time = pygame.time.get_ticks()
        elapsed_time = current_time - start_time

        if elapsed_time < switch_time:
            # First 10 seconds: Mario catches bananas and papers, dodges plastic and bottles
            banana_paper_chance = 7
            plastic_bottle_chance = 3
        else:
            # After 10 seconds: Mario dodges bananas and papers, catches plastic and bottles
            banana_paper_chance = 3
            plastic_bottle_chance = 7

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.VIDEORESIZE:
                screen_width, screen_height = event.w, event.h
                screen = pygame.display.set_mode((screen_width, screen_height), pygame.RESIZABLE)
                background = pygame.transform.scale(background, (screen_width, screen_height))
                x_position = screen_width // 2
                y_position = screen_height - y_offset
                mario_rect.center = (x_position, y_position)

        keys_pressed = pygame.key.get_pressed()
        
        # Horizontal movement
        if keys_pressed[pygame.K_LEFT]:
            x_position -= move_speed
            standing_surface = flipped_standing_surface
            jumping_surface = flipped_jumping_surface
        elif keys_pressed[pygame.K_RIGHT]:
            x_position += move_speed
            standing_surface = pygame.transform.flip(flipped_standing_surface, True, False)
            jumping_surface = pygame.transform.flip(flipped_jumping_surface, True, False)
        else:
            standing_surface = pygame.transform.flip(flipped_standing_surface, True, False)
            jumping_surface = pygame.transform.flip(flipped_jumping_surface, True, False)

        # Ensure Mario stays within the window bounds
        x_position = max(0, min(x_position, screen_width))

        if keys_pressed[pygame.K_SPACE]:
            if not jumping:
                jumping = True
                y_velocity = jump_height
                jump_sound.play() 

        # Spawn falling objects randomly
        if random.randint(0, 100) < banana_paper_chance:
            falling_object = FallingObject(screen_width, 'assets/Banana.webp', (3, 7), "banana_paper")
            falling_objects.add(falling_object)
        if random.randint(0, 100) < plastic_bottle_chance:
            falling_object = FallingObject(screen_width, 'assets/Plastic.webp', (3, 7), "plastic_bottle")
            falling_objects.add(falling_object)
        if random.randint(0, 100) < banana_paper_chance:
            falling_object = FallingObject(screen_width, 'assets/Paper.jfif', (3, 7), "banana_paper")
            falling_objects.add(falling_object)
        if random.randint(0, 100) < plastic_bottle_chance:
            falling_object = FallingObject(screen_width, 'assets/Bottle.webp', (3, 7), "plastic_bottle")
            falling_objects.add(falling_object)


        screen.blit(background, (0, 0))

        if jumping:
            y_position -= y_velocity
            y_velocity -= y_gravity
            if y_velocity < -jump_height:
                jumping = False
            mario_rect = jumping_surface.get_rect(center=(x_position, y_position))
            screen.blit(jumping_surface, mario_rect)
        else:
            mario_rect = standing_surface.get_rect(center=(x_position, y_position))
            screen.blit(standing_surface, mario_rect)

        # Update and draw falling objects
        falling_objects.update()
        falling_objects.draw(screen)

        # Check for collisions with falling objects

        for obj in falling_objects:
            if mario_rect.colliderect(obj.rect):
                if elapsed_time < switch_time:
                    # First 10 seconds: Mario catches bananas and papers, dodges plastic and bottles
                    if obj.object_type == "banana_paper":
                        # Correct object caught
                        score += 1
                        catch_sound.play()  # Play the catch sound
                        falling_objects.remove(obj)  # Remove the object from screen
                    else:
                        # Wrong object caught (Bottle or Plastic when supposed to dodge, or vice versa)
                        lives -= 1
                        catch_sound.play()  # Play the catch sound
                        falling_objects.remove(obj)  # Remove the object from screen
                        if lives <= 0:
                            # Game over if no more lives left
                            game_over_text = font.render("GAME OVER", True, (255, 0, 0))
                            game_over_rect = game_over_text.get_rect(center=(screen_width // 2, screen_height // 2))
                            screen.blit(game_over_text, game_over_rect)
                            pygame.display.update()
                            pygame.time.delay(3000)  # Pause for 3 seconds before quitting
                            pygame.quit()
                            sys.exit()
                else:
                    # After 10 seconds: Mario dodges bananas and papers, catches plastic and bottles
                    if obj.object_type == "banana_paper":
                        # Wrong object caught (Banana or Paper when supposed to dodge, or vice versa)
                        lives -= 1
                        catch_sound.play()  # Play the catch sound
                        falling_objects.remove(obj)  # Remove the object from screen
                        if lives <= 0:
                            #Game over funtion()
                            # Game over if no more lives left
                            game_over_text = font.render("GAME OVER", True, (255, 0, 0))
                            game_over_rect = game_over_text.get_rect(center=(screen_width // 2, screen_height // 2))
                            screen.blit(game_over_text, game_over_rect)
                            pygame.display.update()
                            pygame.time.delay(3000)  # Pause for 3 seconds before quitting
                            pygame.quit()
                            sys.exit()
                    else:
                        # Correct object caught
                        score += 1
                        catch_sound.play()  # Play the catch sound
                        falling_objects.remove(obj)  # Remove the object from screen

        # Display the score on the upper right corner of the screen
        score_text = font.render(f"Score: {score}", True, (255, 255, 255))
        score_rect = score_text.get_rect(topright=(screen_width - 10, 40))
        screen.blit(score_text, score_rect)

        # Display lives as hearts on the upper right corner of the screen
        for i in range(lives):
            screen.blit(heart_image, (screen_width - 40 - i * 40, 10))

        # Calculate and display the timer in the upper middle of the screen
        elapsed_seconds = elapsed_time // 1000
        milliseconds = int((elapsed_time % 1000) / 10)
        timer_text = timer_font.render(f"{elapsed_seconds:02}:{milliseconds:02}", True, timer_color)
        timer_rect = timer_text.get_rect(midtop=(screen_width // 2, 10))  # Adjusted position to the upper middle
        screen.blit(timer_text, timer_rect)

        # Change timer color to black every 10 seconds
        if elapsed_time > switch_time and timer_color != (0, 0, 0):
            timer_color = (0, 0, 0)

        pygame.display.update()
        clock.tick(60)

if __name__ == "__main__":
    run_mario_jump_game()
